const buttonRating = document.querySelectorAll(".button-rating");
buttonRating.forEach((button) => {
  button.addEventListener("click", (event) => {
    const rating = event.target.getAttribute("rating-value");
    buttonRating.forEach((btn) => btn.classList.remove("active"));
    button.classList.add("active");
  });
});

const handleSubmit = () => {
  const rating = document.querySelector(".active");
  if (rating) {
    const ratingValue = rating.getAttribute("rating-value");
    const tyCard = document.getElementById("thankyou-card");
    const ratingCard = document.getElementById("rating-card");
    const summary = document.getElementById("summary");
    ratingCard.style.display = "none";
    tyCard.style.display = "flex";
    summary.innerHTML = `You selected ${ratingValue} out of 5`;
  } else {
    alert("Please select a rating");
  }
};
